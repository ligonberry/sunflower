from rest_framework import serializers

from sunflower.models import CustomUser
from sunflower.models.cookbook import CookBook
from sunflower.models.ingredient import Ingredient
from sunflower.models.product import Product
from sunflower.models.recipe import Recipe
from sunflower.models.recipe_comment import RecipeComment
from sunflower.models.recipe_rating import RecipeRating
from sunflower.models.recipe_step import RecipeStep
from sunflower.models.tag import Tag
from sunflower.services.service.ingredient import IngredientService
from sunflower.services.service.product import ProductService
from sunflower.services.service.recipe_rating import RecipeRatingService


class UserSerializer(serializers.ModelSerializer):
    user_id = serializers.IntegerField(source='id')

    class Meta:
        model = CustomUser
        fields = ('user_id', 'username')


class TagSerializer(serializers.ModelSerializer):
    tag_id = serializers.IntegerField(source='pk', required=False)

    class Meta:
        model = Tag
        fields = ('tag_id', 'title', 'popularity')


class ProductSerializer(serializers.ModelSerializer):
    product_id = serializers.IntegerField(source='pk', required=False)

    class Meta:
        model = Product
        fields = ('name',)


class IngredientSerializer(serializers.ModelSerializer):
    ingredient_id = serializers.IntegerField(source='pk', required=False)

    author = serializers.HiddenField(default=serializers.CurrentUserDefault())
    product = serializers.CharField(source='product.name', default='',
                                    max_length=100)

    class Meta:
        model = Ingredient
        fields = ('ingredient_id', 'quantity', 'weight_measure', 'product',
                  'author')

    def create(self, validated_data):
        kwargs = validated_data.pop("product")
        product, _ = ProductService.get_or_create(kwargs)
        ingredient = Ingredient(product=product, **validated_data)
        return IngredientService.add(ingredient)


class RecipeStepSerializer(serializers.ModelSerializer):
    recipe_step_id = serializers.IntegerField(source='pk', required=False)
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = RecipeStep
        fields = ('recipe_step_id', 'description', 'photo', 'author')


class RecipeCreateUpdateSerializer(serializers.ModelSerializer):
    recipe_id = serializers.IntegerField(source='pk', required=False)
    author = UserSerializer(default=serializers.CurrentUserDefault())

    ingredients = IngredientSerializer(many=True, required=True)
    recipe_steps = RecipeStepSerializer(many=True, required=True)

    class Meta:
        model = Recipe
        fields = ('recipe_id', 'title', 'description', 'photo', 'cooking_time',
                  'dish_weight', 'author', 'ingredients', 'recipe_steps')
        read_only_fields = ('publish_date',)

    def create(self, validated_data):
        rating = RecipeRating()
        added_rating = RecipeRatingService.add(rating)

        ingredients_data = validated_data.pop('ingredients')
        recipe_steps_data = validated_data.pop('recipe_steps')

        recipe = Recipe.objects.create(rating=added_rating, **validated_data)

        for ingredient in ingredients_data:
            serializer = IngredientSerializer()
            ingredient = serializer.create(ingredient)
            recipe.ingredients.add(ingredient)

        for recipe_step in recipe_steps_data:
            serializer = RecipeStepSerializer()
            recipe_step = serializer.create(recipe_step)
            recipe.recipe_steps.add(recipe_step)

        return recipe


class RecipeGetSerializer(serializers.ModelSerializer):
    recipe_id = serializers.IntegerField(source='pk')
    publish_date = serializers.DateField(format="%Y-%m-%d")

    author = UserSerializer(default=serializers.CurrentUserDefault())
    rating = serializers.CharField(source='rating.rating', default=0,
                                   read_only=True)

    class Meta:
        model = Recipe
        fields = ('recipe_id', 'title', 'description', 'photo', 'publish_date',
                  'cooking_time', 'dish_weight', 'rating', 'author')
        read_only_fields = ('publish_date',)


class RecipeCommentSerializer(serializers.ModelSerializer):
    comment_id = serializers.IntegerField(source='pk', required=False)
    author = UserSerializer(default=serializers.CurrentUserDefault())

    class Meta:
        model = RecipeComment
        fields = ('comment_id', 'content', 'photo', 'publish_date', 'author',
                  'recipe')
        read_only_fields = ('publish_date',)


class SubCookBookSerializer(serializers.ModelSerializer):

    class Meta:
        model = CookBook
        fields = ('cookbook_id', 'name')


class CookBookSerializer(serializers.ModelSerializer):
    author = UserSerializer(default=serializers.CurrentUserDefault())

    class Meta:
        model = CookBook
        fields = ('name', 'author')
