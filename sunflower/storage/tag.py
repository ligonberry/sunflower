from sunflower.models.tag import Tag
from sunflower.storage.base_query import BaseQuery


class TagQuery(BaseQuery):

    @staticmethod
    def get_by_title(title: str):
        return Tag.objects.get(title=title)

    @staticmethod
    def get_all_by_recipe_id(recipe_id: int):
        return Tag.objects.filter(recipe__pk=recipe_id)
