"""
    This package provides modules to work with sunflower at a higher level
    (with exception handling).

    Modules:
    ----------------
        * cookbook
            provides functions to work with cookbook data at a higher level
            (with exception handling);
        * cookbook_relation
            provides functions to work with cookbook relation data
            at a higher level (with exception handling);
        * ingredient
            provides functions to work with ingredient data at a higher level
            (with exception handling);
        * product
            provides functions to work with product data at a higher level
            (with exception handling);
        * recipe
            provides functions to work with recipe data at a higher level
            (with exception handling);
        * recipe_comment
            provides functions to work with recipe comment data
            at a higher level (with exception handling);
        * recipe_rating
            provides functions to work with recipe rating data at a higher level
            (with exception handling);
        * recipe_step
            provides functions to work with recipe step data at a higher level
            (with exception handling);
        * tag
            provides functions to work with tag data at a higher level
            (with exception handling);
        * user
            provides functions to work with user data at a higher level
            (with exception handling);

"""

