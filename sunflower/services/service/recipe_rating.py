""" This module provides a class RecipeRatingService for working with recipe
    rating data at a higher level (with error handling)

    Classes:
    ----------------
        RecipeRatingService
            working with recipe rating data in database at a higher level
            (with logical error handling)
"""

from sunflower.models.recipe import Recipe
from sunflower.models.recipe_rating import RecipeRating
from sunflower.services.logger import LogAllMethods
from sunflower.services.service.base_service import BaseService


class RecipeRatingService:
    __metaclass__ = LogAllMethods

    @staticmethod
    def add(recipe_rating: RecipeRating) -> RecipeRating:
        return BaseService.add(recipe_rating)

    @staticmethod
    def get(recipe_rating_id: int) -> RecipeRating:
        return BaseService.get(RecipeRating, recipe_rating_id)

    @staticmethod
    def get_by_recipe_id(recipe_id: int) -> RecipeRating:
        BaseService.is_object_exist(Recipe, recipe_id)
        return RecipeRating.objects.filter(recipe__pk=recipe_id)

    @staticmethod
    def _calculate_new_rating(mark_sum: float, mark_count: int) -> float:
        return mark_sum / mark_count

    @staticmethod
    def update(user_id: int, recipe: Recipe, kwargs) -> RecipeRating:

        mark_sum = kwargs['mark_sum']
        mark_count = kwargs['mark_count']

        rating = RecipeRatingService._calculate_new_rating(mark_sum, mark_count)
        kwargs['rating'] = rating

        return BaseService.update(RecipeRating, user_id, recipe.rating.pk,
                                  kwargs)
