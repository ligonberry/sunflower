# SunFlower
> The best culinary site with which you will cook like a professional 

If you're stuck indoors on a snow day, it's an excellent excuse for getting busy in the kitchen. Save yourself a trip to the shops by baking your own bread, and stay warm with hot drinks, casseroles and soup.

See an API documentations:   
<https://app.swaggerhub.com/apis/ligonberry73/sunflower-api/1.0.0>

![](screenshots/header.jpg)

## Installation
OS X & Linux & Windows:    
``` 1. git clone https://github.com/ligonberry73/Sunflower.git```     
``` 2. In project root dir create file .env```     
``` 3. In the file .env initialize environment variables USERNAME, DBNAME, SECRET_KEY```       
```sh
    USERNAME=test_user
    DBNAME=test_db
    SECRET_KEY=r!idmujce0r-=p(fjl--2!xc#cl$#c1#9$=yupko05(e$q$v&7
```

Free ```Django Key Generator``` - https://www.miniwebtool.com/django-secret-key-generator/

## Usage example

If you want to commit and generate new swagger specification, you should export environment variables  ```USERNAME```, ```DBNAME```, ```SECRET_KEY```.   
Then swagger specification will be create on each commit in file ```data.json```. You can change file name in ```generate_api_doc.py``` 

## Meta

Darya Litvinchuk ```ligonberry@yandex.by```

Distributed under the MIT license. See ``LICENSE`` for more information.


## Contributing

1. Fork it (<https://github.com/ligonberry73/Sunflower/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
